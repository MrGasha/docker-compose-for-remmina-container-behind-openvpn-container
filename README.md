# Description

A docker compose.yml file to deploy the linuxserver/remmina container behind a openvpn client container by dperson/openvpn.
# usage
I deployed the containers using portainer's stack but you can do the same without it, it just makes it a little easier. I will provide instructions for both. <br> 

First you must create two volumes, one for openvpn the other for remmina. The first is to stores your openvpn files, the second is to make your remmina configuration persistent. <br>
## Creating volumes with portainer
On portainer, select the desired environment and then click on Volumes.

![Alt text](images/image.png)

Click on "Add volume"

![Alt text](images/image-1.png)

I will name mine VPN and create volume

![Alt text](images/image-2.png)

Do the same for remmina's volume. Yo should see something like this:

![Alt text](images/image-3.png)
## Creating the volumes via docker commands
To create the volumes  using docker commands you just run:
```bash
docker voume create VPN
```
for the VPN container and
```bash
docker voume create remmina-config
```
for the remmina container. You can verify with:
```bash
docker voume ls
```
It outputs somehting like

![Alt text](images/image-4.png)

You have created the needed volumes
## Preparing .ovpn/.conf file
My openvpn file is .ovpn but you can do the same using a .conf file, the important thing is knowing the authentication method. Mine just asks for a private key, yours could ask for user and password. lets take a look at my .ovpn file:
```ovpn
client
dev tun
proto udp
example.com 1194
resolv-retry infinite
nobind
remote-cert-tls server
tls-version-min 1.2
verify-x509-name device name
cipher AES-256-CBC
auth SHA256
auth-nocache
(...)
```
If your auth method is like mine's (uses a private key password) You need to delete this part:
```ovpn
(...)
auth SHA256
auth-nocache
(...)
```
and add this:
```ovpn
(...)
askpass vpn.pass
(...)
```
then you must create a file named vpn.pass in which you add your private key password.
<br>
If your authentication method asks for user and password the documentations says that you must rename your file to vpn.conf and create a vpn.auth file with your user on the first line and password on the second line see [this tutorial from greenfrog](https://greenfrognest.com/LMDSVPN.php) for details. But roughly what you need to do is add this line to your vpn.conf file:
```ovpn
(...)
auth-user-pass /vpn/vpn.auth
(...)
```
If your .vpn or .conf files has something along this lines:
```ovpn
(...)
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
(...)
```
Remove it

## Uploading the .ovpn/.conf files using Portainer and creating the Stack
To upload the files using portainer you need to navigate again to your environment and select volumes then click on browse button on your VPN volume. if you do not see the browse button [you need an agent ](https://github.com/portainer/portainer/issues/3465)
![Alt text](images/image-5.png)

Click on the upload icon and upload your .ovpn and vpn.pass files one by one

![Alt text](images/image-6.png)

after that, you must see both files on your volume

![Alt text](images/image-7.png)

We are getting closer, now you just need to create the stack, for that navigate to the stack menu:

![Alt text](images/image-8.png)

click and Add stack

![Alt text](images/image-9.png)

Give it a name and select Web editor

![Alt text](images/image-10.png)

On the editor you will paste the content of compose.yml and edit however it suits you (ROUTE, container_names, services, USER, PASSWORD, etc), make sure your ports don't overlap with other containers. <br> 
When you are done editing the compose file you just click on deploy stack button

![Alt text](images/image-11.png)

To confirm that everything went well you must click logs on the vpn container

![Alt text](images/image-12.png)

if your output is similar to this, you are ready to go.
![Alt text](images/image-13.png)
<br> 

## Uploading the .ovpn/.conf files using terminal and running the compose.yml

To add your files to the volumes you created using docker, you have various options, you could transfer the files using sftps, scp, etc. Or you could create the files using your terminal text editor of choice, I am going to do the last. <br>

Go to your VPN volume (or just run nano with the full route) and run nano like this:
```bash
cd /var/lib/docker/volumes/VPN/_data/
nano vpn.ovpn 
```
then paste the content of your .ovpn file and do the same for your vpn.auth or vpn.pass
```bash
nano vpn.pass
```
Create a folder in your wherever you want and create your compose.yml (or clone this repo) like this:

```bash
mkdir openvpn-remmina && cd openvpn-remmina
nano compose.yml 
```
(I did it on my Home folder) if you did nano, just paste the content of the compose, edit however it suits you. <br>

We are ready to run the compose.yml file:
```bash
docker compose.yml up -d
```
It will pull the images and run the containers after that do:
```bash
docker ps
```
to check all the containers are running
it should output something like this:
```bash
CONTAINER ID   IMAGE                    COMMAND                  CREATED              STATUS                            PORTS                                                           NAMES
36a7c7869c94   linuxserver/remmina      "/init"                  About a minute ago   Up 2 seconds                                                                                      remmina
e49a9997757a   dperson/openvpn-client   "/sbin/tini -- /usr/…"   About a minute ago   Up 3 seconds (health: starting)   0.0.0.0:3000-3001->3000-3001/tcp, :::3000-3001->3000-3001/tcp   vpn-gateway

```
finally to check that your vpn container is running well do
```bash
docker logs vpn-gateway
```
your output must be similar to the output on the portainer section above.

## Accessing the remmina app

Navigate to your host's IP:port if is https you must do https://ip:httpsport in my case 3001. <br>
type your user and password

![Alt text](images/image-14.png)

and you are good to go 

![Alt text](images/image-15.png)

Have some questions? [Join the discord server](https://discord.gg/hWUbehteuP)